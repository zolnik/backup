#!/usr/bin/env bash
#
# Backup Valheim server and send notification to Discord chanel.
# by Arkadiusz Zolnik
#
# ROADMAP
# - add checking if server is up and running after backup
# - send backup file to some external storage

WEBHOOK_URL="https://discord.com/api/webhooks/815836036746969098/HyLjXnLzgmRI14X8Bw3pzWL3nK7IKUMQg2SjsW6pOj33-_4xV0LHn_ECtIfxpMtpb6Bd"
LOG_FILE="/home/vhserver/tools/backup/log.txt"

START_MESSAGE="Starting backup. Server will be down for a while."
END_MESSAGE="Backup done. Server up and running. I hope."
ERROR_MESSAGE="Something went wrong and backup failed. I don't know if server is up and running :worried:."

clearLog()
{
    > $LOG_FILE
}

log()
{
    echo "$1"
    echo "$1" >> $LOG_FILE
}

send()
{
    log "DEBUG: inside sent"
    log $1
    log "DISCORD: sending message: $1"
    curl -H "Content-Type: application/json" -H "Expect: application/json" -X POST "${WEBHOOK_URL}" -d '{"content":"'"$1"'"}' 2>/dev/null
    log "DISCORD: message sent."
}

sendResult()
{
    log "DEBUG: Start sendResult"
    success=false
    while read line && [[ $success = false ]]
    do
        if [[ $line =~ "Completed" ]]; then
            log "DEBUG: inside if statement"
            log $line
            log "$line"
            success=true
            message=${line:54}
            log "DEBUG: line: $line"
            log "DEBUG: message: $message"
            log $message
            log "$message"
            send "$message\n$END_MESSAGE"
        fi
    done < $LOG_FILE
    if [ $success = false ] ; then
        send "$ERROR_MESSAGE"
    fi
    log "DEBUG: End sendResult"
}

backup()
{
    log "VHSERVER: starting backup."
    /home/vhserver/vhserver backup 2>&1 >> $LOG_FILE
    log "VHSERVER: backup finished."
}

clearLog
send "$START_MESSAGE"
backup
sendResult